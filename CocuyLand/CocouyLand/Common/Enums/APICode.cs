﻿namespace Common.Enums
{
    public enum APICode
    {
        Ok,
        Fail,
        InternalError,
    }
}