﻿using Common.Entities;
using System.Collections.Generic;

namespace Common.API
{
    public  class RIngredient : REntity
    {
        private string _name;
        private RTypeIngredient _type;
        private List<RCocktail> _cocktails;

        public string Name { get => _name; set => _name = value; }
        public RTypeIngredient Type { get => _type; set => _type = value; }
        public List<RCocktail> Cocktails { get => _cocktails; set => _cocktails = value; }

        public RIngredient() : base() { }

        public static explicit operator RIngredient(Ingredient v)
        {
            RIngredient result = null;
            if(v != null)
            {
                result = new RIngredient();
                result.Id = v.Id;
                result.Name = v.Name;

                if(v.Type != null)
                {
                    result.Type = new RTypeIngredient();
                    result.Type.Id = v.Type.Id;
                    result.Type.Name = v.Type.Name;
                    result.Type.Description = v.Type.Description;
                }

                if(v.Cocktails != null && v.Cocktails.Count > 0)
                {
                    result.Cocktails = new List<RCocktail>();
                    for(int i = 0; i < v.Cocktails.Count; i++)
                    {
                        RCocktail cocktail = new RCocktail();
                        cocktail.Id = v.Cocktails[i].Cocktail.Id;
                        cocktail.Name = v.Cocktails[i].Cocktail.Name;
                        cocktail.Description = v.Cocktails[i].Cocktail.Description;
                        result.Cocktails.Add(cocktail);
                    }
                }
            }
            return result;
        }
    }
}