﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.API
{
    public class Response<Object>
    {
        private APICode _statusAPI;
        private string _message;
        private Object _entity;
        private List<Object> _entities;

        public APICode StatusAPI { get => _statusAPI; set => _statusAPI = value; }
        public string Message { get => _message; set => _message = value; }
        public Object Entity { get => _entity; set => _entity = value; }
        public List<Object> Entities { get => _entities; set => _entities = value; }

        public Response() { }
    }
}
