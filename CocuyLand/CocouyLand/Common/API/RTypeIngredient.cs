﻿using Common.Entities;
using System.Collections.Generic;

namespace Common.API
{
    public class RTypeIngredient : REntity
    {
        private string _name;
        private string _description;
        private List<RIngredient> _ingredients;

        public string Name { get => _name; set => _name = value; }
        public string Description { get => _description; set => _description = value; }
        public List<RIngredient> Ingredients { get => _ingredients; set => _ingredients = value; }

        public RTypeIngredient() : base() { }

        public static explicit operator RTypeIngredient(TypeIngredient v)
        {
            RTypeIngredient result = null;
            if(v!= null)
            {
                result = new RTypeIngredient();
                result.Id = v.Id;
                result.Name = v.Name;
                result.Description = v.Description;
                if(v.Ingredients != null && v.Ingredients.Count > 0)
                {
                    result.Ingredients = new List<RIngredient>();
                    for(int i = 0; i < v.Ingredients.Count; i++)
                    {
                        RIngredient ingredient = new RIngredient();
                        ingredient.Id = v.Ingredients[i].Id;
                        ingredient.Name = v.Ingredients[i].Name;
                        result.Ingredients.Add(ingredient);
                    }
                }
            }
            return result;
        }
    }
}