﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.API
{
    public class RMeasure : REntity
    {
        private string _name;
        private string _acronym;

        public string Name { get => _name; set => _name = value; }
        public string Acronym { get => _acronym; set => _acronym = value; }

        public RMeasure() : base() { }

        public static explicit operator RMeasure(Measure v)
        {
            RMeasure result = null;
            if(v != null)
            {
                result = new RMeasure();
                result.Id = v.Id;
                result.Name = v.Name;
                result.Acronym = v.Acronym;
            }
            return result;
        }
    }
}
