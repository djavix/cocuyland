﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.API
{
    public class RCocktail : REntity
    {
        private string _name;
        private string _preparation;
        private string _description;
        private string _photo;
        private List<RCocktailIngredient> _ingredients;

        public string Name { get => _name; set => _name = value; }
        public string Preparation { get => _preparation; set => _preparation = value; }
        public string Description { get => _description; set => _description = value; }
        public string Photo { get => _photo; set => _photo = value; }
        public List<RCocktailIngredient> Ingredients { get => _ingredients; set => _ingredients = value; }

        public RCocktail() : base() { }

        public static explicit operator RCocktail(Cocktail v)
        {
            RCocktail result = null;
            if(v != null)
            {
                result = new RCocktail();
                result.Id = v.Id;
                result.Name = v.Name;
                result.Preparation = v.Preparation;
                result.Description = v.Description;
                result.Photo = v.Photo;

                if(v.Ingredients != null && v.Ingredients.Count > 0)
                {
                    result.Ingredients = new List<RCocktailIngredient>();
                    for (int i = 0; i < v.Ingredients.Count; i++)
                    {
                        RCocktailIngredient cocktailIngredient = new RCocktailIngredient();
                        cocktailIngredient.Quantity = v.Ingredients[i].Quantity;

                        RIngredient ingredient = new RIngredient();
                        ingredient.Id = v.Ingredients[i].Ingredient.Id;
                        ingredient.Name = v.Ingredients[i].Ingredient.Name;
                        cocktailIngredient.Ingredient = ingredient;

                        RMeasure measure = new RMeasure();
                        measure.Id = v.Ingredients[i].Measure.Id;
                        measure.Name = v.Ingredients[i].Measure.Name;
                        measure.Acronym = v.Ingredients[i].Measure.Acronym;
                        cocktailIngredient.Measure = measure;

                        result.Ingredients.Add(cocktailIngredient);
                    }

                }
            }
            return result;
        }
    }
}
