﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.API
{
    public class RCocktailIngredient : REntity
    {
        private float _quantity;
        private RIngredient _ingredient;
        private RMeasure _measure;
        private RCocktail _cocktail;

        public float Quantity { get => _quantity; set => _quantity = value; }
        public RIngredient Ingredient { get => _ingredient; set => _ingredient = value; }
        public RMeasure Measure { get => _measure; set => _measure = value; }
        public RCocktail Cocktail { get => _cocktail; set => _cocktail = value; }

        public RCocktailIngredient() : base() { }

        public static explicit operator RCocktailIngredient(CocktailIngredient v)
        {
            RCocktailIngredient result = null;
            if(v != null)
            {
                result = new RCocktailIngredient();
                result.Quantity = v.Quantity;

                if(v.Ingredient != null)
                {
                    result.Ingredient = new RIngredient();
                    result.Ingredient.Id = v.Ingredient.Id;
                    result.Ingredient.Name = v.Ingredient.Name;
                    if (v.Ingredient.Type != null)
                    {
                        result.Ingredient.Type = new RTypeIngredient();
                        result.Ingredient.Type.Id = v.Ingredient.Type.Id;
                        result.Ingredient.Type.Name = v.Ingredient.Type.Name;
                        result.Ingredient.Type.Description = v.Ingredient.Type.Description;
                    }
                }

                if(v.Measure != null)
                {
                    result.Measure = new RMeasure();
                    result.Measure.Id = v.Measure.Id;
                    result.Measure.Name = v.Measure.Name;
                    result.Measure.Acronym = v.Measure.Acronym;
                }

                if(v.Cocktail != null)
                {
                    result.Cocktail = new RCocktail();
                    result.Cocktail.Id = v.Cocktail.Id;
                    result.Cocktail.Name = v.Cocktail.Name;
                    result.Cocktail.Preparation = v.Cocktail.Preparation;
                    result.Cocktail.Description = v.Cocktail.Description;
                }
            }
            return result;
        }
    }
}
