﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Cocktail : Entity
    {
        private string _name;
        private string _preparation;
        private string _description;
        private string _photo;
        private IList<CocktailIngredient> _ingredients;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Preparation { get => _preparation; set => _preparation = value; }
        public virtual string Description { get => _description; set => _description = value; }
        public virtual string Photo { get => _photo; set => _photo = value; }
        public virtual IList<CocktailIngredient> Ingredients { get => _ingredients; set => _ingredients = value; }

        public Cocktail() : base() { }
    }
}
