﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Ingredient : Entity
    {
        private string _name;
        private TypeIngredient _type;
        private IList<CocktailIngredient> _cocktails;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual TypeIngredient Type { get => _type; set => _type = value; }
        public virtual IList<CocktailIngredient> Cocktails { get => _cocktails; set => _cocktails = value; }

        public Ingredient() : base() { }
    }
}
