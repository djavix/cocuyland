﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class User : Entity
    {
        private string _nickname;
        private string _password;
        private string _name;
        private string _email;
        private string _phone;
        private string _photo;
        private string _biography;
        private IList<Commentary> _commentaries;
        private IList<Post> _posts;
        private IList<LikePost> _likePosts;
        private IList<LikeCommentary> _likeCommentaries;

        public virtual string Nickname { get => _nickname; set => _nickname = value; }
        public virtual string Password { get => _password; set => _password = value; }
        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Email { get => _email; set => _email = value; }
        public virtual string Phone { get => _phone; set => _phone = value; }
        public string Photo { get => _photo; set => _photo = value; }
        public virtual string Biography { get => _biography; set => _biography = value; }
        public virtual IList<Commentary> Commentaries { get => _commentaries; set => _commentaries = value; }
        public virtual IList<Post> Posts { get => _posts; set => _posts = value; }
        public virtual IList<LikePost> LikePosts { get => _likePosts; set => _likePosts = value; }
        public virtual IList<LikeCommentary> LikeCommentaries { get => _likeCommentaries; set => _likeCommentaries = value; }
        

        public User() : base() { }
    }
}
