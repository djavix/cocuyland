﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Post : Entity
    {
        private DateTime _date;
        private Cocktail _cocktail;
        private User _user;
        private IList<Commentary> _commentaries;
        private IList<LikePost> _likes;

        public virtual DateTime Date { get => _date; set => _date = value; }
        public virtual Cocktail Cocktail { get => _cocktail; set => _cocktail = value; }
        public virtual User User { get => _user; set => _user = value; }
        public virtual IList<Commentary> Commentaries { get => _commentaries; set => _commentaries = value; }
        public virtual IList<LikePost> Likes { get => _likes; set => _likes = value; }

        public Post() : base() { }
    }
}
