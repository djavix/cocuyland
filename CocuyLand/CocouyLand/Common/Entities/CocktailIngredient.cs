﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class CocktailIngredient : Entity
    {
        private float _quantity;
        private Ingredient _ingredient;
        private Measure _measure;
        private Cocktail _cocktail;

        public virtual float Quantity { get => _quantity; set => _quantity = value; }
        public virtual Ingredient Ingredient { get => _ingredient; set => _ingredient = value; }
        public virtual Measure Measure { get => _measure; set => _measure = value; }
        public virtual Cocktail Cocktail { get => _cocktail; set => _cocktail = value; }

        public CocktailIngredient() : base() { }


    }
}
