﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Commentary : Entity
    {
        private string _comment;
        private DateTime _date;
        private Post _post;
        private User _user;
        private IList<LikeCommentary> _likes;


        public virtual string Comment { get => _comment; set => _comment = value; }
        public virtual DateTime Date { get => _date; set => _date = value; }
        public virtual Post Post { get => _post; set => _post = value; }
        public virtual User User { get => _user; set => _user = value; }
        public virtual IList<LikeCommentary> Likes { get => _likes; set => _likes = value; }

        public Commentary() : base() { }
    }
}
