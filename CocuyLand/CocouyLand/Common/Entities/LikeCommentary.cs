﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class LikeCommentary : Entity
    {
        private Commentary _commentary;
        private User _user;

        public virtual Commentary Commentary { get => _commentary; set => _commentary = value; }
        public virtual User User { get => _user; set => _user = value; }

        public LikeCommentary() : base() { }
    }
}
