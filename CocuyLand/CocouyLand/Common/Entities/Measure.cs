﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Measure : Entity
    {
        private string _name;
        private string _acronym;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Acronym { get => _acronym; set => _acronym = value; }

        public Measure() : base() { }
    }
}
