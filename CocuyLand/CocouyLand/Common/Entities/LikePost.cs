﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class LikePost : Entity
    {
        private Post _post;
        private User _user;

        public virtual Post Post { get => _post; set => _post = value; }
        public virtual User User { get => _user; set => _user = value; }

        public LikePost() : base() { }
    }
}
