﻿using DAO.DAOS;
using DAO.IDAOS;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO
{
    public static class Factory
    {
        public static ICocktail GetDAOCocktail()
        {
            return new Cocktail();
        }

        public static ICocktailIngredient GetDAOCocktailIngredient()
        {
            return new CocktailIngredient();
        }

        public static ICommentary GetDAOCommentary()
        {
            return new Commentary();
        }

        public static IIngredient GetDAOIngredient()
        {
            return new Ingredient();
        }

        public static ILikeCommentary GetDAOLikeCommentary()
        {
            return new LikeCommentary();
        }

        public static ILikePost GetDAOLikePost()
        {
            return new LikePost();
        }

        public static IMeasure GetDAOMeasure()
        {
            return new Measure();
        }

        public static IPost GetDAOPost()
        {
            return new Post();
        }

        public static ITypeIngredient GetDAOTypeIngredient()
        {
            return new TypeIngredient();
        }

        public static IUser GetDAOUser()
        {
            return new User();
        }
    }
}
