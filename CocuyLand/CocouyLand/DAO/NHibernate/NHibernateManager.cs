﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Reflection;

namespace DAO.NHibernate
{
    internal class NHibernateManager : IDisposable
    {
        #region Atributos
        private static ISessionFactory _sessionFactory;
        public ISession Session;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public NHibernateManager()
        {
            CrearSession();
        }

        #endregion

        #region Metodos Privados
        /// <summary>
        /// Metodo para crear la session de NHibernate
        /// </summary>
        private void CrearSession()
        {
            if (_sessionFactory == null)
            {
                Configuration configuration = new Configuration().Configure();
                configuration.AddAssembly(Assembly.GetCallingAssembly());
                _sessionFactory = configuration.BuildSessionFactory();
            }

            Session = OpenSession();

        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// Metodo para abrir la session de NHibernate
        /// </summary>
        /// <returns></returns>
        public static ISession OpenSession()
        {
            return _sessionFactory.OpenSession();
        }

        /// <summary>
        /// Metodo para cerrar la session de NHibernate
        /// </summary>
        public static void CloseSession()
        {
            _sessionFactory.Dispose();
        }

        #endregion

        public void Dispose()
        {
            CloseSession();
        }
    }
}
