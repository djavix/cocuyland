﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.IDAOS
{
    public interface IBase<TEntity>
    {
        /// <summary>
        /// Firma de metodo agregar
        /// </summary>
        /// <param name="entity">endidad agregar</param>
        /// <returns>booleano de la operacion</returns>
        bool Add(TEntity entity);
        /// <summary>
        /// Firma de metodo actualiuzar
        /// </summary>
        /// <param name="entity">entidad actualizar</param>
        /// <returns>booleano de la operacion </returns>
        bool Update(TEntity entity);
        /// <summary>
        /// Firma de metodo eliminar
        /// </summary>
        /// <param name="entity">entidad a eliminar</param>
        /// <returns>booleano de la operacion</returns>
        bool Delete(TEntity entity);
        /// <summary>
        /// Firma de metodo para buscar por id
        /// </summary>
        /// <param name="id">id de la entidad a buscar</param>
        /// <returns>entidad solicitada</returns>
        TEntity SearchById(TEntity entity);
        /// <summary>
        /// Firma de metodos para buscar todos
        /// </summary>
        /// <returns>listado de la entidad indicada</returns>
        IList<TEntity> SearchAll();
    }
}
