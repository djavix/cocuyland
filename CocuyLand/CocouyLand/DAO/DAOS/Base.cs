﻿using Common.Entities;
using DAO.IDAOS;
using DAO.NHibernate;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.DAOS
{
    /// <summary>
    /// Clase base para el manejo de CRUD implementando la interfaz IBase
    /// </summary>
    /// <typeparam name="TEntity">Variable generica representando una Entidad</typeparam>
    internal class Base<TEntity> : IBase<TEntity> where TEntity : Entity
    {
        #region Variables

        public readonly ISession _session;
        private ITransaction _transaction;

        #endregion

        #region Constructor
        /// <summary>
        /// Contructor de la clase
        /// </summary>
        public Base()
        {
            _session = new NHibernateManager().Session;
        }

        #endregion
        /// <summary>
        /// Implementacion del metodo Add de la interfaz IBase
        /// salva la enidad especificada en la base de datos
        /// </summary>
        /// <param name="entity">entidad agregar</param>
        /// <returns>booleano de la operacion</returns>
        public bool Add(TEntity entity)
        {
            bool result = false;
            try
            {
                _transaction = _session.BeginTransaction();
                _session.Save(entity);
                _transaction.Commit();
                result = entity.Id > 0;
            }
            catch (Exception e)
            {
                _transaction.Rollback();
                throw e;
            }
            return result;
        }
        /// <summary>
        /// Implementacion del metodo Delete de la interfaz IBase
        /// borra de la base de datos la entidad indicada
        /// </summary>
        /// <param name="entity">entidad a eliminar</param>
        /// <returns>booleano de la operacion</returns>
        public bool Delete(TEntity entity)
        {
            bool result = false;
            try
            {
                _transaction = _session.BeginTransaction();
                _session.Delete(entity);
                _transaction.Commit();
                result = true;
            }
            catch (Exception e)
            {
                _transaction.Rollback();
                throw e;
            }
            return result;
        }
        /// <summary>
        /// Implementacion del metodo SearchAll de la interfaz IBase
        /// busca todos los registro de la entidad especificada
        /// </summary>
        /// <returns>Listado de entidad</returns>
        public IList<TEntity> SearchAll()
        {
            IList<TEntity> result = null;
            try
            {
                result = _session.CreateCriteria(typeof(TEntity)).List<TEntity>();

            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        /// <summary>
        /// Implementacion del metodo SearchById de la interfaz IBase
        /// busca la entidad indicada segun el id
        /// </summary>
        /// <param name="id">id de la entidad a buscar</param>
        /// <returns>Entidad solicitada</returns>
        public TEntity SearchById(TEntity entity)
        {
            TEntity result = null;
            try
            {
                result = _session.Get<TEntity>(entity.Id);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        /// <summary>
        /// Implementacion del metodo Update de la interfaz IBase
        /// actualiza la informacion de la entidad especificada
        /// </summary>
        /// <param name="entity">entidad a modificar</param>
        /// <returns>booleano de la operacion</returns>
        public bool Update(TEntity entity)
        {
            bool result = false;
            try
            {
                _transaction = _session.BeginTransaction();
                _session.Update(entity);
                _transaction.Commit();
                result = true;
            }
            catch (Exception e)
            {
                _transaction.Rollback();
                throw e;
            }
            return result;
        }
    }
}
