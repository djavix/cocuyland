﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    /// <summary>
    /// Clase base para la implementacion de comando realizados hacia la base de datos.
    /// </summary>
    /// <typeparam name="I">Parametro de entrada</typeparam>
    /// <typeparam name="O">Parametro de salida</typeparam>
    public abstract class Command<I, O>
    {
        protected I _input;
        protected O _output;

        /// <summary>
        /// Propiedad para asignar la variable de entrada
        /// </summary>
        public I Input { set => _input = value; }
        /// <summary>
        /// Propieda para poder obtener la salida de ejecucion
        /// </summary>
        public O Output { get => _output; }

        /// <summary>
        /// Firma de metodo para mandar a ejecutar el comando
        /// </summary>
        public abstract void Execute();
    }
}
